﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HellfireStudios.StarboundPlanetTracker
{
    public class StarboundResourceGrabber
    {
        // D:\SteamLibrary\SteamApps\common\Starbound\assets\celestial
        // planet resource copy.
        //D:\SteamLibrary\SteamApps\common\Starbound\assets\celestial\system\gas_giant
        private void GetGasImages()
        { }
        private void GetPlanetImages()
        { }
        private void GetSectorImages()
        { }

        // TODO: future!
        //[Obsolete]
        //private void GetLandscapeImages()
        //{ }
        //[Obsolete]
        //private void GetLandscapeImages2()
        //{ }

        private void CreateImageDirectoryStructure()
        {
            // one dir for each biome in here
            HellfireADK.Directories.EnsureDirectoryWasCreated(HellfireADK.Applications.AssemblyDirectory + @"\Images\planet\biomes");

            HellfireADK.Directories.EnsureDirectoryWasCreated(HellfireADK.Applications.AssemblyDirectory + @"\Images\planet\overlays");
            HellfireADK.Directories.EnsureDirectoryWasCreated(HellfireADK.Applications.AssemblyDirectory + @"\Images\planet\patterns");

            // TODO: future!
            // HellfireADK.Directories.EnsureDirectoryWasCreated(HellfireADK.Applications.AssemblyDirectory + @"\Images\planet\landscapes");
        }
    }
}
