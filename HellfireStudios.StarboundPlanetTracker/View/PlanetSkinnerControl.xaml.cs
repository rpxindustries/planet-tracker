﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HellfireStudios.StarboundPlanetTracker.Model;
using System.ComponentModel;

namespace HellfireStudios.StarboundPlanetTracker.View
{
    /// <summary>
    /// Interaction logic for PlanetSkinnerControl.xaml
    /// </summary>
    public partial class PlanetSkinnerControl : UserControl
    {
        public PlanetSkinnerControl()
        {
            InitializeComponent();
        }
        private PlanetSkin _Skin;
        private UIPlanetSkinner _Skinner;

        public PlanetSkin Skin
        {
            get { return _Skin; }
            set { _Skin = value; OnSkinChanged(value); }
        }

        private void OnSkinChanged(PlanetSkin value)
        {
            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                this._Skinner = new Model.UIPlanetSkinner(HellfireADK.Applications.AssemblyDirectory + @"\Images\planet");
                this._Skinner.Design = value;

                if (value != null)
                {
                    if (!String.IsNullOrEmpty(this._Skin.BiomeName))
                        this.PlanetBiomeImage.Source = this._Skinner.GetBiomeBitmap();

                    if (!String.IsNullOrEmpty(this._Skin.PatternName))
                        this.PlanetOverlayImage.Source = this._Skinner.GetPatternBitmap();

                    if (!String.IsNullOrEmpty(this._Skin.OverlayName))
                        this.PlanetPatternImage.Source = this._Skinner.GetOverlayBitmap();
                }
            }
        }
        

    }
}