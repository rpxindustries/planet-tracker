﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HellfireStudios.StarboundPlanetTracker.Model;

namespace HellfireStudios.StarboundPlanetTracker.View
{
    /// <summary>
    /// Interaction logic for aml
    /// </summary>
    public partial class PlanetViewer : Window
    {
        private Planet planet;
        private bool _IsEditing;

        public PlanetViewer(Planet row, bool editing = false)
        {
            InitializeComponent();
            this._IsEditing = editing;
            this.PlanetNameTextBox.IsReadOnly = editing;
            this.planet = row;
            this.DataTextBox.Text = row.ToString();
            this.NotesTextBox.Text = row.Notes.Replace("|",Environment.NewLine);
            //this.planetImage.Background = new ImageBrush(new BitmapImage(new Uri(
            //    String.Format(HellfireADK.Applications.AssemblyDirectory + "/Resources/PlanetImages/sector/{0}.png",
            //    this.planet.Sector))));

            this.PlanetNameTextBox.Text = row.Name;

            this.SectorImage.Background = new ImageBrush(new BitmapImage(new Uri(
                String.Format(HellfireADK.Applications.AssemblyDirectory+"/Images/sector/{0}.png",
                this.planet.Sector))));

            this.PlanetSkinnerControl.Skin = row.Skin;
            if (this.planet.Skin == null) 
                this.planet.Skin = new PlanetSkin();
            //this.planetImage.Background

        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            this.planet.Notes = this.NotesTextBox.Text;
            this.planet.Name = this.PlanetNameTextBox.Text;

            if (this._IsEditing)
            {
                var dict = this.planet.ToDictionary();
                dict.Remove("Name");
                App.Database.Update("planets", dict, String.Format("Name='{0}'",planet.Name));
            }
            else
            {
                App.Database.Insert("planets", this.planet.ToDictionary());
            }
            this.Close();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if(e.LeftButton == MouseButtonState.Pressed)
                DragMove();
        }

        private void PlanetImageLabel_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var skin = PlanetSkinner.GetSkin(this.planet.Skin);

            if (skin != null) this.planet.Skin = skin;
            this.PlanetSkinnerControl.Skin = skin;
        }
    }
}

