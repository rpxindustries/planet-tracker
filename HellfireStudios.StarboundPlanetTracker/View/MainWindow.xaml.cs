﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using HellfireStudios.StarboundPlanetTracker.Model;
using HellfireStudios.StarboundPlanetTracker.Properties;
using HellfireStudios.StarboundPlanetTracker.View;
using Microsoft.Win32;
using HellfireADK.Sqlite;
using System.IO;

namespace HellfireStudios.StarboundPlanetTracker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Members and Properties
        // Members
        private bool _SortAscending;

        // Properties
        private UIPlanetTracker trackerUI { get; set; }
        public Planet SelectedPlanet { get; set; }
        public Planet SelectedBookmark { get; set; }
        #endregion

        #region Construction
        public MainWindow()
        {
            InitializeComponent();

            if (String.IsNullOrEmpty(Settings.Default.StarboundFolder))
            {
                var frm = new View.Setup();
                frm.ShowDialog();
                if (frm.DialogResult.HasValue && !frm.DialogResult.Value == true)
                {
                    this.Close();
                }
                else
                {
                    Settings.Default.Save();
                }
            }

            // Create the smart UI object
            this.trackerUI = new UIPlanetTracker();
            try
            {
                App.Database = SqliteDatabase.CreateSqliteDatabase(App.DatabaseDefaultPath, Legacy.Static.GetSchemaDict(Legacy.Static.V2DATABASESCHEMA));
                // Try to load the book marks, will be legacy if we fail.
                this.trackerUI.LoadBookmarks(App.Database);
            }
            catch (Exception)
            {
                if (MessageBox.Show("There was an error processing your database. If you have just upgraded to a new version click OK to upgrade your database. (A backup will be created)", "New version check", MessageBoxButton.OKCancel, MessageBoxImage.Error) == MessageBoxResult.OK)
                {
                    UpgradeLegacyDatabase();
                    this.trackerUI.LoadBookmarks(App.Database);
                }
                // TODO: Error handling here.
            }
            this.trackerUI.FindAndLoadPlanets(Settings.Default.StarboundFolder.TrimEnd('\\') + @"\universe\");

            RefreshGridViewDisplay(this.trackerUI.Bookmarks,this.BooksmarksDataGrid);
            RefreshGridViewDisplay(this.trackerUI.PlanetsFound, this.SearchDataGrid);
#if DEBUG
            this.SettingsMenuRoot.Visibility = System.Windows.Visibility.Visible;
#endif
        }

        private void UpgradeLegacyDatabase()
        {
            var v1 = new Legacy.Version1Database(App.DatabaseDefaultPath);
            App.Database = v1.ConvertToVersion2();
        }
        #endregion

        #region Pages
        #region Bookmark Page
        /// <summary>Controls enabling of buttons and selected bookmark member</summary>
        private void BooksmarksDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.SelectedBookmark = UpdateSelectedItem(BooksmarksDataGrid.SelectedItem);
        }
        private void RemoveBookmarkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.SelectedBookmark != null)
            {
                if (MessageBox.Show("Are you sure?", "Last chance!", MessageBoxButton.YesNo, MessageBoxImage.Stop) == MessageBoxResult.Yes)
                {
                    App.Database.Delete("planets", String.Format("Name='{0}'", this.SelectedBookmark.Name));
                    this.trackerUI.Bookmarks.Remove(SelectedBookmark);
                }
            }
            RefreshGridViewDisplay(trackerUI.Bookmarks,BooksmarksDataGrid);
        }
        private void EditBookmarkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.SelectedBookmark != null)
            {
                var frm = new PlanetViewer(this.SelectedBookmark, true);
                frm.ShowDialog();
            }
            RefreshGridViewDisplay(trackerUI.Bookmarks, BooksmarksDataGrid);
        }
        #endregion
        #region Search Page
        /// <summary>Controls enabling of buttons and selected bookmark member</summary>
        private void SearchDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.SelectedPlanet = UpdateSelectedItem(SearchDataGrid.SelectedItem);
            if (this.SelectedPlanet != null)
            {
                this.SelectedPlanetLabel.Content = this.SelectedPlanet.Name;
            }
        }
        /// <summary>Shows the add planet dialog box.</summary>
        private void AddPlanetToBookmarksButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.SelectedPlanet != null)
            {
                var frm = new PlanetViewer(this.SelectedPlanet);
                frm.ShowDialog();
            }
            this.trackerUI.Bookmarks = UIPlanetTracker.LoadBookmarkedPlanets(App.Database);
            RefreshGridViewDisplay(trackerUI.Bookmarks, BooksmarksDataGrid);
        }
        /// <summary>Loads planets from a folder into the DataGrid</summary>
        private void AutoFindButton_Click(object sender, RoutedEventArgs e)
        {
            this.trackerUI.FindAndLoadPlanets();
            RefreshGridViewDisplay(this.trackerUI.PlanetsFound, this.SearchDataGrid);
        }
        #endregion
        /// <summary>Updates the currently selected bookmark or found planet.</summary>
        /// <param name="selected">The property to set</param>
        /// <param name="selectedItem">The grid row to convert to a planet</param>
        private Planet UpdateSelectedItem(object selectedItem)
        {
            this.AddToDbButton.IsEnabled = SearchDataGrid.SelectedItem != null;
            this.RemoveBookmarkButton.IsEnabled = BooksmarksDataGrid.SelectedItem != null;
            this.EditBookmarkButton.IsEnabled = BooksmarksDataGrid.SelectedItem != null;

            if (selectedItem != null)
            {
                return (Planet)selectedItem;
            }
            return null;
        }
        /// <summary>Loads a list of planets into the given grid view.</summary>
        /// <param name="planets">The list of planets to load.</param>
        /// <param name="grid">The grid to load the planets into.</param>
        private void RefreshGridViewDisplay(List<Planet> planets,DataGrid grid)
        {
            var planetList = new ListCollectionView(planets);
            planetList.GroupDescriptions.Add(new PropertyGroupDescription("Planet"));
            grid.ItemsSource = planetList;
        }
        /// <summary>Controls the column sorting of the grid views.</summary>
        private void DataGridColumnSort_Sorting(object sender, DataGridSortingEventArgs e)
        {
            e.Handled = true;
            var dataGrid = (DataGrid)sender;
            var column = dataGrid.Columns[e.Column.DisplayIndex];

            // Clear current sort descriptions
            dataGrid.Items.SortDescriptions.Clear();

            // Add the new sort description
            dataGrid.Items.SortDescriptions.Add(new SortDescription(column.SortMemberPath, _SortAscending ? ListSortDirection.Ascending : ListSortDirection.Descending));

            // Apply sort
            foreach (var col in dataGrid.Columns)
            {
                col.SortDirection = null;
            }
            column.SortDirection = _SortAscending ? ListSortDirection.Ascending : ListSortDirection.Descending;

            // Refresh items to display sort
            dataGrid.Items.Refresh();
            _SortAscending = !_SortAscending;
        }
        #endregion

        #region Menu
        #region File Menu
        private void MenuItemLoadDatabase_Click(object sender, RoutedEventArgs e)
        {
            LoadDatabase();
        }
        private void LoadDatabase()
        {
            this.trackerUI = new UIPlanetTracker();
            this.trackerUI.Bookmarks = UIPlanetTracker.LoadBookmarkedPlanets(App.Database);
            RefreshGridViewDisplay(this.trackerUI.Bookmarks, this.BooksmarksDataGrid);
        }

        private void MenuItemClearDatabase_Click(object sender, RoutedEventArgs e)
        {
            this.trackerUI.DeleteAllBookmarks();
        }

        private void MenuItemSaveDatabase_Click(object sender, RoutedEventArgs e)
        {
            if (!CopyDatabase())
            {
                MessageBox.Show("There was an error saving, please try again!");
            }
        }
        private bool CopyDatabase()
        {
            MessageBox.Show("Please select a location to save the database to in the following dialog box.", "Select save path...");

            var dlg = new SaveFileDialog();
            if (dlg.ShowDialog().HasValue)
            {
                System.IO.File.Copy("planets.sqlite", dlg.FileName);
                if (System.IO.File.Exists(dlg.FileName))
                {
                    return true;
                }
            }
            return false;
        }

        private void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        #endregion
        #region HelpMenu
        private void MenuItemAbout_Click(object sender, RoutedEventArgs e)
        {
            new AboutForm().ShowDialog();
        }
        #endregion

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        #endregion

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                this.DragMove();
            }
        }

        private void SettingsMenuRoot_Click(object sender, RoutedEventArgs e)
        {
            var sb = new StringBuilder();

            sb.AppendFormat("Settings.Default.StarboundFolder :: {1}''{0}''{1}{1}", Settings.Default.StarboundFolder, Environment.NewLine);
            sb.AppendFormat("Settings.Default.LastDirectoryAdded :: {1}''{0}''{1}{1}", Settings.Default.LastDirectoryAdded, Environment.NewLine);
            sb.AppendLine("Settings.Default.ColumnIgnoreList: ");
            if (Settings.Default.ColumnIgnoreList != null)
            {
                var x = new List<string>(Settings.Default.ColumnIgnoreList.Cast<string>());
                x.ForEach(i =>
                    {
                        sb.AppendFormat("{0},", i);
                    }
                    );
                sb.Remove(sb.Length - 1, 1);
            }
            else
            {
                sb.AppendLine("[None]");
            }
            MessageBox.Show(sb.ToString());
        }

        private void BooksmarksDataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (Settings.Default.ColumnIgnoreList != null && Settings.Default.ColumnIgnoreList.Contains(e.PropertyName))
            {
                e.Cancel = true;
            }
        }

        private void MenuItemConvert_Click(object sender, RoutedEventArgs e)
        {
            UpgradeLegacyDatabase();
        }

        private void ResetWindowStateButton_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Normal;
        }

        private void Window_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Maximized;
        }

        // TODO: Add this stuff back in to the XAML, placehodler there now.
        //<DataGrid.ContextMenu>
        //                    <ContextMenu>
        //                        <MenuItem Header="Add Column" MaxHeight="25">
        //                            <MenuItem.Icon>
        //                                <Image Source="/HellfireStudios.StarboundPlanetTracker;component/Images/add1.png" Width="25" Height="25"/>
        //                            </MenuItem.Icon>
        //                        </MenuItem>
        //                        <MenuItem Header="Remove Column" MaxHeight="25">
        //                            <MenuItem.Icon>
        //                                <Image Source="/HellfireStudios.StarboundPlanetTracker;component/Images/remove.png" Width="25" Height="25"/>
        //                            </MenuItem.Icon>
        //                        </MenuItem>
        //                        <MenuItem Header="Sort Column" MaxHeight="25">
        //                            <MenuItem.Icon>
        //                                <Image Source="/HellfireStudios.StarboundPlanetTracker;component/Images/sort.png" Width="25" Height="25"/>
        //                            </MenuItem.Icon>
        //                        </MenuItem>
        //                    </ContextMenu>
        //                </DataGrid.ContextMenu>
    }
}
