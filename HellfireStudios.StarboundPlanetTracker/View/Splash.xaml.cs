﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HellfireStudios.StarboundPlanetTracker.View
{
    /// <summary>
    /// Interaction logic for Splash.xaml
    /// </summary>
    public partial class Splash : Window
    {
        private int imageAnimationFrame = 0;
        private int stopSlash = 0;

        public Splash()
        {
            InitializeComponent();
            this.Images = new List<string>(); 

            for(int i = 0; i < 10; i++)
            {
                this.Images.Add(i + ".png");
            }
            this._Timer = new DispatcherTimer();
            this._Timer.Tick += new EventHandler(_Timer_Tick);
            this._Timer.Interval = new TimeSpan(0, 0, 0, 0, 270);
            this._Timer.Start();
        }

        void _Timer_Tick(object sender, EventArgs e)
        {
            var splashImage = HellfireADK.Applications.AssemblyDirectory + @"\Images\Splash\" + this.Images[imageAnimationFrame];
            SplashImage.Source = new BitmapImage(new Uri(splashImage));

            // Animation controls
            imageAnimationFrame++;
            if (imageAnimationFrame == 9)
            {
                imageAnimationFrame = 0;
                stopSlash++;
                if (stopSlash == 2)
                {
                    this.Visibility = System.Windows.Visibility.Hidden;
                    this._Timer.Stop();
                    new MainWindow().Show();
                    this.Close();
                }
            }
        }

        public List<string> Images { get; set; }

        public DispatcherTimer _Timer { get; set; }
    }
}
