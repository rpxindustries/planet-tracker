﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using HellfireStudios.StarboundPlanetTracker.Properties;

namespace HellfireStudios.StarboundPlanetTracker.View
{
    /// <summary>
    /// Interaction logic for Setup.xaml
    /// </summary>
    public partial class Setup : Window
    {
        public Setup()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            this.textBox1.Text = GetFolderPathFromDialog();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            if (Directory.Exists(this.textBox1.Text))
            {
                Settings.Default.StarboundFolder = this.textBox1.Text;
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("The directory does not exist!");
            }
        }

        public static string GetFolderPathFromDialog()
        {
            using (var dlg = new System.Windows.Forms.FolderBrowserDialog())
            {
                dlg.ShowNewFolderButton = true;
                System.Windows.Forms.DialogResult result = dlg.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    return dlg.SelectedPath;
                }
            }
            return null;
        }
    }
}
