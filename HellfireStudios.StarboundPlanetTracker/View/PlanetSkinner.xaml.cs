﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HellfireStudios.StarboundPlanetTracker.Model;

namespace HellfireStudios.StarboundPlanetTracker.View
{
    /// <summary>
    /// Interaction logic for PickPlanet.xaml
    /// </summary>
    public partial class PlanetSkinner : Window
    {
        public UIPlanetSkinner UIPlanetSkinner { get; set; }
        
        public PlanetSkinner(PlanetSkin skin = null)
        {
            InitializeComponent();
            this.UIPlanetSkinner = new Model.UIPlanetSkinner(HellfireADK.Applications.AssemblyDirectory+@"\Images\planet");

            this.BiomesComboBox.ItemsSource = this.UIPlanetSkinner.Biomes;
            this.PatternsComboBox.ItemsSource = this.UIPlanetSkinner.Patterns;
            this.OverlayComboBox.ItemsSource = this.UIPlanetSkinner.Overlays;

            this.UIPlanetSkinner.Design = skin;

            if(skin !=null)
            {
                if (!String.IsNullOrEmpty(skin.BiomeName))
                    this.BiomesComboBox.SelectedItem = skin.BiomeName;
                if (!String.IsNullOrEmpty(skin.OverlayName))
                    this.OverlayComboBox.SelectedItem = skin.OverlayName;
                if (!String.IsNullOrEmpty(skin.PatternName))
                    this.PatternsComboBox.SelectedItem = skin.PatternName;
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            //this.Close();
        }

        private void UpdateSkinPreview()
        {
            if (!String.IsNullOrEmpty(this.UIPlanetSkinner.Design.BiomeName))
                this.PlanetBiomeImage.Source = this.UIPlanetSkinner.GetBiomeBitmap();
            else
                this.PlanetPatternImage.Source = null;

            if (!String.IsNullOrEmpty(this.UIPlanetSkinner.Design.PatternName))
                this.PlanetOverlayImage.Source = this.UIPlanetSkinner.GetPatternBitmap();
            else
                this.PlanetPatternImage.Source = null;

            if (!String.IsNullOrEmpty(this.UIPlanetSkinner.Design.OverlayName))
                this.PlanetPatternImage.Source = this.UIPlanetSkinner.GetOverlayBitmap();
            else
                this.PlanetPatternImage.Source = null;
        }

        private void BiomesComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.BiomesComboBox.SelectedItem != null)
            {
                this.UIPlanetSkinner.Design.BiomeName = BiomesComboBox.SelectedItem.ToString();
                UpdateSkinPreview();
            }
        }

        private void PatternsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.PatternsComboBox.SelectedItem != null)
            {
                this.UIPlanetSkinner.Design.PatternName = PatternsComboBox.SelectedItem.ToString();
                UpdateSkinPreview();
            }
        }

        private void OverlayComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.OverlayComboBox.SelectedItem != null)
            {
                this.UIPlanetSkinner.Design.OverlayName = OverlayComboBox.SelectedItem.ToString();
                UpdateSkinPreview();
            }
        }

        internal static PlanetSkin GetSkin(PlanetSkin skin)
        {
            var frm = new PlanetSkinner(skin);

            frm.ShowDialog();
            if (frm.DialogResult.HasValue && frm.DialogResult.Value)
            {
                return frm.UIPlanetSkinner.Design;
            }
            return null;
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            this.BiomesComboBox.SelectedItem = null;
            this.PatternsComboBox.SelectedItem = null;
            this.OverlayComboBox.SelectedItem = null;
            this.UIPlanetSkinner.Design = new PlanetSkin();
            UpdateSkinPreview();
        }
    }
}
