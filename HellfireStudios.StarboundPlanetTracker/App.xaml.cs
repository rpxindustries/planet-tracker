﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using HellfireADK.Sqlite;
using System.IO;
using System.Windows.Threading;
using HellfireStudios.StarboundPlanetTracker.Properties;
using System.Text;

namespace HellfireStudios.StarboundPlanetTracker
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static SqliteDatabase Database { get; set; }
        public static string DatabaseDefaultPath { get; private set; }

        [Obsolete("Do not use")]
        public static Dictionary<string, string> GetDatabaseSchema()
        {
            var schema = new Dictionary<string, string>();
            schema.Add("planets", @"('Name' TEXT PRIMARY KEY NOT NULL UNIQUE, 'Sector' TEXT, 'X' TEXT, 'Y' TEXT, 'SystemID' TEXT, 'DistanceFromSun' TEXT, 'DistanceFromPlanet' TEXT, 'LastVisitDate' TEXT, 'RoughSize' TEXT, 'Notes' TEXT, 'Skin' TEXT)");
            return schema;
        }

        public App()
        {
            App.DatabaseDefaultPath = HellfireADK.Applications.AssemblyDirectory + @"\planets.sqlite";
            App.Current.DispatcherUnhandledException += new DispatcherUnhandledExceptionEventHandler(Current_DispatcherUnhandledException);
            App.Current.Exit += new ExitEventHandler(Current_Exit);
        }

        void Current_Exit(object sender, ExitEventArgs e)
        {
            Settings.Default.Save();
        }

        void Current_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            var sb = new StringBuilder();
            sb.AppendLine(String.Format("'{1}'{0}", Environment.NewLine, e.Exception.Message));
            sb.AppendLine(String.Format("Source:'{1}'{0}", Environment.NewLine, e.Exception.Source));
            sb.AppendLine(String.Format("Trace:{0}'{1}'",Environment.NewLine,e.Exception.StackTrace));
            MessageBox.Show(e.Exception.Message);
            try
            { File.WriteAllText(HellfireADK.Applications.AssemblyDirectory, sb.ToString()); }
            catch { }

        }


    }
}
