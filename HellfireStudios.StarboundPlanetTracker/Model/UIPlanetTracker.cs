﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.Data;
using HellfireADK.Sqlite;
using System.Windows;
using HellfireStudios.StarboundPlanetTracker.Properties;
using System.IO;

namespace HellfireStudios.StarboundPlanetTracker.Model
{
    public class UIPlanetTracker
    {
        /// <summary>A list of planets loaded from a file path search.</summary>
        private List<Planet> _PlanetsFound;
        public List<Planet> PlanetsFound
        {
            get { return _PlanetsFound; }
            set { _PlanetsFound = value; }
        }

        /// <summary>A list of bookmarked planets, this is saved to the database.</summary>
        public List<Planet> Bookmarks
        {
            get { return _Bookmarks; }
            set { _Bookmarks = value; }
        }
        private List<Planet> _Bookmarks;

        public UIPlanetTracker() 
        { 
            this._PlanetsFound = new List<Planet>();
            this._Bookmarks = new List<Planet>();
        }

        public void FindAndLoadPlanets(string filePath = null)
        {
            // TODO: Fix this fuck up... For later use with pre selecting a folder to browse from.
            // filePath = string.Empty;

            var planets = new List<Planet>();
            // If the filePath is null get the user to file it.
            if (String.IsNullOrEmpty(filePath))
            {
                filePath = GetFolderPathFromDialog();
            }
            if (!String.IsNullOrEmpty(filePath))
            {
                var fileList = new System.IO.DirectoryInfo(filePath).GetFiles().ToList();
                
                foreach (var file in fileList)
                {
                    if (file.Extension == ".world")
                        planets.Add(new Planet(file));

                    // For later use with pre selecting a folder to browse from.
                    Settings.Default.LastDirectoryAdded = filePath;
                }
            }

            this._PlanetsFound = planets;
        }
        public static List<Planet> LoadBookmarkedPlanets(SqliteDatabase db)
        {
            var planets = new List<Planet>();
            foreach (DataRow datarow in db.GetData("SELECT * FROM planets").Rows)
            {
                planets.Add(new Planet(datarow));
            }

            return planets;
        }
        public static Planet LoadPlanet(string filePath = null)
        {
            if (String.IsNullOrEmpty(filePath))
                filePath = UIPlanetTracker.GetFilePathFromDialog<OpenFileDialog>();

            return new Planet(new System.IO.FileInfo(filePath));
        }

        /// <summary>Shows the find file dialog box configured to find *.world files.</summary>
        /// <returns>Either a file path or null if the operation failed.</returns>
        public static string GetFilePathFromDialog<T>() where T :  Microsoft.Win32.FileDialog, new()
        {
            var dlg = new T();
                dlg.Filter = "World files (*.world)|*.world|All files (*.*)|*.*";
                dlg.ShowDialog();
                if (!string.IsNullOrEmpty(dlg.FileName))
                    return dlg.FileName;
                else
                    return null;
        }
        /// <summary>Prompts the user to select a folder.</summary>
        /// <returns>Either a folder path or null if the operation failed.</returns>
        public static string GetFolderPathFromDialog()
        {            
            using (var dlg = new System.Windows.Forms.FolderBrowserDialog())
            {
                dlg.ShowNewFolderButton = true;
                System.Windows.Forms.DialogResult result = dlg.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    return dlg.SelectedPath;                    
                }
            }
            return null;
        }

        internal void DeleteAllBookmarks()
        {
            if (MessageBox.Show("Are you sure you wish to delete all stored planet data? THERE IS NO WAY TO REVERSE THIS!", "Last chance..", MessageBoxButton.YesNo, MessageBoxImage.Hand) == MessageBoxResult.Yes)
            {
                try
                {
                    App.Database.EmptyTable("planets");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Table empty failed: " + ex.Message);
                }
            }
        }

        internal void LoadBookmarks(SqliteDatabase sqliteDatabase)
        {
            this.Bookmarks = UIPlanetTracker.LoadBookmarkedPlanets(App.Database);
        }
    }
}
