﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HellfireStudios.StarboundPlanetTracker.Model
{
    public class PlanetCoordinates
    {
        private string _X;
        public string X
        {
            get { return _X; }
            set { _X = value; }
        }
        private string _Y;
        public string Y
        {
            get { return _Y; }
            set { _Y = value; }
        }
               
        public PlanetCoordinates(string xCoordinate, string yCoordinate)
        {
            this._X = xCoordinate;
            this._Y = yCoordinate;
        }
        public override string ToString()
        {
            return string.Format("{0},{1}",this._X,this._Y);
        }
        public Dictionary<string,object> ToDictionary()
        {
            return new Dictionary<string, object>() { { "X", this._X }, { "Y", this._Y } };
        }


        internal static PlanetCoordinates Parse(string p)
        {
            var split = p.Split(',');
            return new PlanetCoordinates(split[0], split[1]);
        }
    }
}
