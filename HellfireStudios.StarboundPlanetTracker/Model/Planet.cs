﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.IO;

namespace HellfireStudios.StarboundPlanetTracker.Model
{
    public class Planet
    {
        private string filePath = string.Empty;

        private int ID { get; set; }

        public string Name { get; set; }

        public PlanetCoordinates Coordinates { get; set; }

        public string Sector { get; set; }

        public string SystemId { get; set; }

        public string DistanceFromSun { get; set; }

        public string DistanceFromPlanet { get; set; }

        public string Notes { get; set; }

        public string RoughSize { get; set; }

        public string LastVisitDate { get; set; }

        public PlanetSkin Skin { get; set; }

        #region Construction
        public Planet(FileInfo file)
        {
            var fullPlanetDataString = Path.GetFileNameWithoutExtension(file.FullName);
            var split = fullPlanetDataString.Split('_');

            this.Name = fullPlanetDataString;
            Array.Resize(ref split, 6);

            //Sector is first
            this.Sector = split[0];

            // Next planet coords
            this.Coordinates = new PlanetCoordinates(split[1], split[2]);

            // Solar System ID
            this.SystemId = split[3];

            // Distance of planet from the systems sun.
            this.DistanceFromSun = split[4];

            // If 0 This is a Planet, otherwise distance of moon from planet.
            this.DistanceFromPlanet = split[5]==null ? string.Empty : split[5];

            // Last time the file was written to.
            this.LastVisitDate = file.LastWriteTime.ToString();

            // Size of the file
            this.RoughSize = file.Length.ToString();

            // Planets image
            this.Skin = new PlanetSkin();

            // Notes about the planet
            this.Notes = string.Empty;
        }

        public Planet()
        {
            this.Coordinates = new PlanetCoordinates("x", "y");
            this.LastVisitDate = string.Empty;
            this.Name = string.Empty;
            this.RoughSize = string.Empty;
            this.Sector = string.Empty;
            this.SystemId = string.Empty;
            this.DistanceFromSun = string.Empty;
            this.DistanceFromPlanet = string.Empty;
            this.Notes = string.Empty;
            this.Skin = new PlanetSkin();
        }
        public Planet(System.Data.DataRow datarow)
        {
            this.Coordinates = new PlanetCoordinates(datarow["X"].ToString(), datarow["Y"].ToString());
            this.LastVisitDate = datarow["LastVisitDate"].ToString() ?? string.Empty;
            this.Name = datarow["Name"].ToString() ?? string.Empty;
            this.RoughSize = datarow["RoughSize"].ToString() ?? string.Empty;
            this.Sector = datarow["Sector"].ToString() ?? string.Empty;

            this.SystemId = datarow["SystemId"].ToString() ?? string.Empty;

            this.DistanceFromSun = datarow["DistanceFromSun"].ToString();
            if (String.IsNullOrEmpty(DistanceFromSun)) DistanceFromSun = string.Empty;
            this.DistanceFromPlanet = datarow["DistanceFromPlanet"].ToString();
            if (String.IsNullOrEmpty(DistanceFromPlanet)) DistanceFromPlanet = string.Empty;

            this.Notes = datarow["Notes"].ToString().Replace("@c01;","'") ?? string.Empty;
            this.Skin = PlanetSkin.GetPlanetSkinFromString(datarow["Skin"].ToString());
            if (this.Skin == null) this.Skin = new PlanetSkin();
        }
        #endregion

        internal Dictionary<string, object> ToDictionary()
        {
            // Add coords
            var dict = this.Coordinates.ToDictionary();
            // Add image
            dict.Add("Skin", this.Skin.ToString());

            // Add the rest!
            foreach (var info in this.GetType().GetProperties())
            {
                if (info.Name == "Notes")
                    Notes = Notes.Replace(Environment.NewLine, "|").Replace("'","@c01;");
                if (info.Name != "Coordinates" && info.Name != "Skin") 
                    dict.Add(info.Name, info.GetValue(this,null));
            }
            return dict;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine("Name:");
            sb.AppendLine(this.Name);
            sb.AppendLine("LastVisitDate:");
            sb.AppendLine(this.LastVisitDate);
            sb.AppendLine("- - - - -");
            sb.AppendLine("Coordinates:");
            sb.AppendLine(this.Coordinates.ToString());
            sb.AppendLine("Sector:");
            sb.AppendLine(this.Sector);
            sb.AppendLine("Solar System ID:");
            sb.AppendLine(this.SystemId);
            sb.AppendLine("Distance From Sun:");
            sb.AppendLine(this.DistanceFromSun);
            sb.AppendLine("Distance From Planet:");
            sb.AppendLine(this.DistanceFromPlanet);
            sb.AppendLine("- - - - -");
            sb.AppendLine("Skin:");
            sb.AppendLine(this.Skin.ToString());
            sb.AppendLine("Notes:");
            sb.AppendLine(this.Notes.ToString());
            return sb.ToString();
        }
    }
}
