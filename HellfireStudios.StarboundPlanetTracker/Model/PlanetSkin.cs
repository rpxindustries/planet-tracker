﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace HellfireStudios.StarboundPlanetTracker.Model
{
    /// <summary>
    /// Immutable? Holds a planets design.
    /// </summary>
    public class PlanetSkin
    {
        private const char SEPERATOR = ';';

        private string _BiomeName = null;
        private string _PatternName = null;
        private string _OverlayName = null;
        private int _Width = 50;
        private int _Height = 50;

        public string BiomeName
        {
            get { return this._BiomeName; }
            set { this._BiomeName = value; } 
        }
        public string PatternName
        {
            get { return this._PatternName; }
            set { this._PatternName = value; }
        }
        public string OverlayName
        {
            get { return this._OverlayName; }
            set { this._OverlayName = value; }
        }
        public int Width
        {
            get { return this._Width; }
            set { this._Width = value; }
        }
        public int Height
        {
            get { return this._Height; }
            set { this._Height = value; }
        }

        public PlanetSkin()
        { 
        
        }

        public static PlanetSkin GetPlanetSkinFromString(string design)
        {
            var skin = new PlanetSkin();

            var split = design.Split(SEPERATOR);
            skin.BiomeName = split[0];
            skin.PatternName = split[1];
            skin.OverlayName = split[2];
            skin.Height = int.Parse(split[3]);
            skin.Width = int.Parse(split[4]);

            return skin;
        }

        public override string ToString()
        {
            return string.Join(SEPERATOR.ToString(),
                this._BiomeName, 
                this._PatternName,
                this._OverlayName, 
                this._Height.ToString(),
                this._Width.ToString());
        }
    }
}