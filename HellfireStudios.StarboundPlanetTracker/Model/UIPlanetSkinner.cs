﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace HellfireStudios.StarboundPlanetTracker.Model
{
    public class UIPlanetSkinner
    {
        private List<string> _PatternImagePaths = null;
        private List<string> _OverlayImagePaths = null;
        private List<string> _BiomeImagePaths = null;
        private string _ImageRootPath = string.Empty;

        public PlanetSkin Design { get; set; }
        public List<string> Biomes { get; set; }
        public List<string> Patterns { get; set; }
        public List<string> Overlays { get; set; }

        public UIPlanetSkinner(string ImageRootPath)
        {
            this.Design = new PlanetSkin();
            this._ImageRootPath = ImageRootPath;

            LoadImagePaths();
            LoadNamesLists();
        }

        private void LoadNamesLists()
        {
            this.Biomes = Directory.GetDirectories(_ImageRootPath+@"\Biomes\").Select(i=> i.Substring(i.LastIndexOf(Path.DirectorySeparatorChar)+1)).ToList();
            this.Patterns = this._PatternImagePaths.Select(s => s.Substring(s.LastIndexOf(Path.DirectorySeparatorChar)+1).Replace(".png","")).ToList();
            this.Overlays = this._OverlayImagePaths.Select(s => s.Substring(s.LastIndexOf(Path.DirectorySeparatorChar)+1).Replace(".png","")).ToList();
        }

        private void LoadImagePaths()
        {
            this._BiomeImagePaths = new List<string>();
            foreach (var dir in Directory.GetDirectories(this._ImageRootPath + @"\Biomes\"))
            {
                this._BiomeImagePaths.AddRange(Directory.GetFiles(dir));
            }

            this._PatternImagePaths = Directory.GetFiles(this._ImageRootPath + @"\Patterns\").ToList();
            this._OverlayImagePaths = Directory.GetFiles(this._ImageRootPath + @"\Overlays\").ToList();
        }

        internal BitmapImage GetPatternBitmap()
        {
            return GetBitmapImage(GetUri("patterns", Design.PatternName, "png"));
        }

        internal BitmapImage GetOverlayBitmap()
        {
            return GetBitmapImage(GetUri("overlays", Design.OverlayName, "png"));
        }

        internal BitmapImage GetBiomeBitmap()
        {
            return GetBitmapImage(GetUri("biomes", Design.BiomeName + @"\maskie1", "png"));
        }

        /// <summary>Constructs a Uri from provided parameters.</summary>
        /// <param name="layer">The type of layer</param>
        /// <param name="name">Layer name</param>
        /// <param name="extension">Layer image extension</param>
        /// <returns></returns>
        private Uri GetUri(string layer, string name, string extension)
        {
                var imagePath = String.Format(@"{0}\{1}\{2}\{3}.{4}",
                HellfireADK.Applications.AssemblyDirectory,
                @"Images\planet",
                layer,
                name,
                extension
                );
            return new Uri(imagePath,UriKind.Absolute);
        }
        /// <summary>Gets a BitmapImage which has some initial properties set to fulfil the bitmap image request.</summary>
        /// <param name="uri">The Uri to set as the image source</param>
        /// <returns>A new bitmap image with initial properties set.</returns>
        private BitmapImage GetBitmapImage(Uri uri)
        {
            var bit = new BitmapImage();
            bit.BeginInit();
            bit.UriSource = uri;

            // Set height off design
            //bit.DecodePixelWidth = Design.Width;
            //bit.DecodePixelHeight = Design.Width;
            bit.EndInit();
            return bit;
        }
    }
}
