﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HellfireADK.Sqlite;
using System.IO;
using System.Data;
using HellfireStudios.StarboundPlanetTracker.Model;

namespace HellfireStudios.StarboundPlanetTracker.Legacy
{
    public class Version1Database : ILegacyDatabaseProcessor
    {

        private SqliteDatabase _db;
        public SqliteDatabase Database
        {
            get { return _db; }
            set { _db = value; }
        }
        public Version1Database(string path)
        {
            this._db = SqliteDatabase.CreateSqliteDatabase(path, Static.GetSchemaDict(Static.V1DATABASESCHEMA));
        }
        public SqliteDatabase ConvertToVersion1()
        {
            return this._db;
        }

        public SqliteDatabase ConvertToVersion2()
        {
            return Convert1To2();
            //else
            //{
            //    throw new InvalidOperationException("Failed to convert database of this type to version 2.");
            //}
            return null;
        }

        private SqliteDatabase Convert1To2()
        {
            string path = HellfireADK.Applications.AssemblyDirectory + @"\planets.sqlite";
            if (File.Exists(path))
            {
                // make a backup
                File.Copy(path, path + ".backup",true);

                // get temp to work on and remove selected tb
                File.Copy(path, path + ".temp", true);

                // get v1 db
                var v1db = this._db;

                if (v1db != null)
                {
                    // load old data into datatable
                    var olddata = v1db.GetData("SELECT * FROM planets");

                    // cleanup v1
                    v1db = null;
                    File.Delete(path);

                    // create the v2 db with schema constant
                    var v2db = SqliteDatabase.CreateSqliteDatabase(path, Static.GetSchemaDict(Static.V2DATABASESCHEMA));

                    // parse each row
                    foreach (DataRow row in olddata.Rows)
                    {
                        // split unknown column
                        // TODO: Fix this clusterfuck of messyness. command pattern maybs?
                        var unknown2 = row["Unknown2"].ToString().Split(',') ?? new string[1];
                        Array.Resize(ref unknown2, 2);
                        var distsun = unknown2[0];
                        var distplan = unknown2[1];
                        if (string.IsNullOrEmpty(distsun)) distsun = "";
                        if (string.IsNullOrEmpty(distplan)) distplan = "";

                        // create planet
                        var planet = new Planet()
                        {
                            Coordinates = new PlanetCoordinates(row["X"].ToString(), row["Y"].ToString()),
                            DistanceFromPlanet = distplan,
                            DistanceFromSun = distsun,
                            LastVisitDate = row["LastVisitDate"].ToString() ?? string.Empty,
                            Name = row["Name"].ToString() ?? string.Empty,
                            Notes = row["Notes"].ToString().Replace("@c01;", "'") ?? string.Empty,
                            RoughSize = row["RoughSize"].ToString() ?? string.Empty,
                            Sector = row["Sector"].ToString() ?? string.Empty,
                            SystemId = row["Unknown1"].ToString() ?? string.Empty,
                        };

                        // get skin
                        planet.Skin = PlanetSkin.GetPlanetSkinFromString(row["Skin"].ToString());
                        if (planet.Skin == null)
                            planet.Skin = new PlanetSkin();

                        v2db.Insert("planets", planet.ToDictionary());
                    }
                    // cleanup
                    v1db = null;
                    // delete temp file
                    File.Delete(path + ".temp");

                    // return the new db
                    return v2db;
                }
            }
            return null;
        }

        public SqliteDatabase ConvertToVersion3(ILegacyDatabaseProcessor oldDb)
        {
            throw new InvalidOperationException("Cannot current convert from version 1 to version 3.");
        }
    }
}
