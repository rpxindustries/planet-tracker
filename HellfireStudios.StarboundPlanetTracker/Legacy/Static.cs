﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HellfireStudios.StarboundPlanetTracker.Legacy
{
    internal static class Static
    {
        public const string V1DATABASESCHEMA = @"('Name' TEXT PRIMARY KEY NOT NULL UNIQUE, 'Sector' TEXT, 'X' TEXT, 'Y' TEXT, 'Unknown1' TEXT, 'Unknown2' TEXT, 'LastVisitDate' TEXT, 'RoughSize' TEXT, 'Notes' TEXT, 'Skin' TEXT)";
        public const string V2DATABASESCHEMA = @"('Name' TEXT PRIMARY KEY NOT NULL UNIQUE, 'Sector' TEXT, 'X' TEXT, 'Y' TEXT, 'SystemID' TEXT, 'DistanceFromSun' TEXT, 'DistanceFromPlanet' TEXT, 'LastVisitDate' TEXT, 'RoughSize' TEXT, 'Notes' TEXT, 'Skin' TEXT)";

        public static Dictionary<string, string>  GetSchemaDict(string schema)
        {
            var dict = new Dictionary<string, string>();
            dict.Add("planets", schema);
            return dict;
        }
    }
}
