﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HellfireADK.Sqlite;
using System.IO;
using System.Data;
using HellfireStudios.StarboundPlanetTracker.Model;

namespace HellfireStudios.StarboundPlanetTracker.Legacy
{
    public class Version2Database : ILegacyDatabaseProcessor
    {
        public Version2Database(string path)
        {
            this._db = SqliteDatabase.CreateSqliteDatabase(path, Static.GetSchemaDict(Static.V2DATABASESCHEMA));
        }
        private SqliteDatabase _db;
        public SqliteDatabase Database
        {
            get { return _db; }
            set { _db = value; }
        }

        public SqliteDatabase ConvertToVersion1()
        {
            throw new InvalidOperationException("Cannot current convert from version 2 to version 1.");
        }

        public SqliteDatabase ConvertToVersion2()
        {
            return this._db;
        }

        public SqliteDatabase ConvertToVersion3(ILegacyDatabaseProcessor oldDb)
        {
            throw new InvalidOperationException("Cannot current convert from version 2 to version 3.");
        }
    }
}
