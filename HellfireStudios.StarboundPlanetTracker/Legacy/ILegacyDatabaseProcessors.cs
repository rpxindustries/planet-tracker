﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HellfireADK.Sqlite;

namespace HellfireStudios.StarboundPlanetTracker.Legacy
{
    public interface ILegacyDatabaseProcessor
    {
        SqliteDatabase Database { get; set; }

        /// <summary>
        /// Converts the databse to a version1 db
        /// </summary>
        /// <returns>A version 1 sqlite database</returns>
        SqliteDatabase ConvertToVersion1();
        /// <summary>
        /// Converts the databse to a version2 db
        /// </summary>
        /// <returns>A version 2 sqlite database</returns>
        SqliteDatabase ConvertToVersion2();
        /// <summary>
        /// Converts the databse to a version3 db
        /// </summary>
        /// <returns>A version 3 sqlite database</returns>
        SqliteDatabase ConvertToVersion3(ILegacyDatabaseProcessor oldDb);
    }
}
