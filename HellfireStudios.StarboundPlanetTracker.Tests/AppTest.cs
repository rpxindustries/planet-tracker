﻿using HellfireStudios.StarboundPlanetTracker;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using HellfireADK.Sqlite;

namespace HellfireStudios.StarboundPlanetTracker.Tests
{
    
    
    /// <summary>
    ///This is a test class for AppTests and is intended
    ///to contain all AppTests Unit Tests
    ///</summary>
    [TestClass()]
    public class AppTests
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Database
        ///</summary>
        [TestMethod()]
        public void DatabaseShouldNotReturnNull()
        {
            var expected = SqliteDatabase.CreateSqliteDatabase("test.sqlite",null); // TODO: Initialize to an appropriate value
            App.Database = expected;
            var actual = App.Database;
            Assert.ReferenceEquals(expected, actual);
        }


        /// <summary>
        ///A test for App Constructor
        ///</summary>
        [TestMethod()]
        public void AppConstructorShouldNotReturnNull()
        {
            App target = new App();
            Assert.IsNotNull(target);
        }
    }
}
