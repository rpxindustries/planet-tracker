﻿using HellfireStudios.StarboundPlanetTracker.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data;
using System.IO;
using System.Collections.Generic;

namespace HellfireStudios.StarboundPlanetTracker.Tests
{
    
    
    /// <summary>
    ///This is a test class for PlanetTests and is intended
    ///to contain all PlanetTests Unit Tests
    ///</summary>
    [TestClass()]
    public class PlanetTests
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Planet Constructor
        ///</summary>
        [TestMethod()]
        public void PlanetConstructorShould()
        {
            DataRow datarow = null; // TODO: Initialize to an appropriate value
            Planet target = new Planet(datarow);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for Planet Constructor
        ///</summary>
        [TestMethod()]
        public void PlanetConstructorShould1()
        {
            Planet target = new Planet();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for Planet Constructor
        ///</summary>
        [TestMethod()]
        public void PlanetConstructorShould2()
        {
            FileInfo file = null; // TODO: Initialize to an appropriate value
            Planet target = new Planet(file);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for ToDictionary
        ///</summary>
        [TestMethod()]
        public void ToDictionaryShould()
        {
            Planet target = new Planet(); // TODO: Initialize to an appropriate value
            Dictionary<string, object> expected = null; // TODO: Initialize to an appropriate value
            Dictionary<string, object> actual;
            actual = target.ToDictionary();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ToString
        ///</summary>
        [TestMethod()]
        public void ToStringShould()
        {
            Planet target = new Planet(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.ToString();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Coordinates
        ///</summary>
        [TestMethod()]
        public void CoordinatesShould()
        {
            Planet target = new Planet(); // TODO: Initialize to an appropriate value
            PlanetCoordinates expected = null; // TODO: Initialize to an appropriate value
            PlanetCoordinates actual;
            target.Coordinates = expected;
            actual = target.Coordinates;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for LastVisitDate
        ///</summary>
        [TestMethod()]
        public void LastVisitDateShould()
        {
            Planet target = new Planet(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.LastVisitDate = expected;
            actual = target.LastVisitDate;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Name
        ///</summary>
        [TestMethod()]
        public void NameShould()
        {
            Planet target = new Planet(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.Name = expected;
            actual = target.Name;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Notes
        ///</summary>
        [TestMethod()]
        public void NotesShould()
        {
            Planet target = new Planet(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.Notes = expected;
            actual = target.Notes;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for RoughSize
        ///</summary>
        [TestMethod()]
        public void RoughSizeShould()
        {
            Planet target = new Planet(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.RoughSize = expected;
            actual = target.RoughSize;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Sector
        ///</summary>
        [TestMethod()]
        public void SectorShould()
        {
            Planet target = new Planet(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.Sector = expected;
            actual = target.Sector;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Skin
        ///</summary>
        [TestMethod()]
        public void SkinShould()
        {
            Planet target = new Planet(); // TODO: Initialize to an appropriate value
            PlanetSkin expected = null; // TODO: Initialize to an appropriate value
            PlanetSkin actual;
            target.Skin = expected;
            actual = target.Skin;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Unknown1
        ///</summary>
        [TestMethod()]
        public void Unknown1Should()
        {
            Planet target = new Planet(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.SystemId = expected;
            actual = target.SystemId;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Unknown2
        ///</summary>
        [TestMethod()]
        public void Unknown2Should()
        {
            Planet target = new Planet(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.DistanceFromSun = expected;
            actual = target.DistanceFromSun;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
