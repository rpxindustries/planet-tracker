﻿using HellfireStudios.StarboundPlanetTracker.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace HellfireStudios.StarboundPlanetTracker.Tests
{
    
    
    /// <summary>
    ///This is a test class for PlanetCoordinatesTests and is intended
    ///to contain all PlanetCoordinatesTests Unit Tests
    ///</summary>
    [TestClass()]
    public class PlanetCoordinatesTests
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for PlanetCoordinates Constructor
        ///</summary>
        [TestMethod()]
        public void PlanetCoordinatesConstructorShould()
        {
            string xCoordinate = string.Empty; // TODO: Initialize to an appropriate value
            string yCoordinate = string.Empty; // TODO: Initialize to an appropriate value
            PlanetCoordinates target = new PlanetCoordinates(xCoordinate, yCoordinate);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for Parse
        ///</summary>
        [TestMethod()]
        public void ParseShould()
        {
            string p = string.Empty; // TODO: Initialize to an appropriate value
            PlanetCoordinates expected = null; // TODO: Initialize to an appropriate value
            PlanetCoordinates actual;
            actual = PlanetCoordinates.Parse(p);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ToDictionary
        ///</summary>
        [TestMethod()]
        public void ToDictionaryShould()
        {
            string xCoordinate = string.Empty; // TODO: Initialize to an appropriate value
            string yCoordinate = string.Empty; // TODO: Initialize to an appropriate value
            PlanetCoordinates target = new PlanetCoordinates(xCoordinate, yCoordinate); // TODO: Initialize to an appropriate value
            Dictionary<string, object> expected = null; // TODO: Initialize to an appropriate value
            Dictionary<string, object> actual;
            actual = target.ToDictionary();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ToString
        ///</summary>
        [TestMethod()]
        public void ToStringShould()
        {
            string xCoordinate = string.Empty; // TODO: Initialize to an appropriate value
            string yCoordinate = string.Empty; // TODO: Initialize to an appropriate value
            PlanetCoordinates target = new PlanetCoordinates(xCoordinate, yCoordinate); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.ToString();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for X
        ///</summary>
        [TestMethod()]
        public void XShould()
        {
            string xCoordinate = string.Empty; // TODO: Initialize to an appropriate value
            string yCoordinate = string.Empty; // TODO: Initialize to an appropriate value
            PlanetCoordinates target = new PlanetCoordinates(xCoordinate, yCoordinate); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.X = expected;
            actual = target.X;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Y
        ///</summary>
        [TestMethod()]
        public void YShould()
        {
            string xCoordinate = string.Empty; // TODO: Initialize to an appropriate value
            string yCoordinate = string.Empty; // TODO: Initialize to an appropriate value
            PlanetCoordinates target = new PlanetCoordinates(xCoordinate, yCoordinate); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.Y = expected;
            actual = target.Y;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
