﻿using HellfireStudios.StarboundPlanetTracker;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Markup;
using System.Windows;
using System.Windows.Controls;

namespace HellfireStudios.StarboundPlanetTracker.Tests
{
    
    
    /// <summary>
    ///This is a test class for NumberUpDownTests and is intended
    ///to contain all NumberUpDownTests Unit Tests
    ///</summary>
    [TestClass()]
    public class NumberUpDownTests
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for NumberUpDown Constructor
        ///</summary>
        [TestMethod()]
        public void NumberUpDownConstructorShould()
        {
            NumberUpDown target = new NumberUpDown();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        public void InitializeComponentShould()
        {
            NumberUpDown target = new NumberUpDown(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for System.Windows.Markup.IComponentConnector.Connect
        ///</summary>
        [TestMethod()]
        [DeploymentItem("HellfireStudios.StarboundPlanetTracker.exe")]
        public void ConnectShould()
        {
            IComponentConnector target = new NumberUpDown(); // TODO: Initialize to an appropriate value
            int connectionId = 0; // TODO: Initialize to an appropriate value
            object target1 = null; // TODO: Initialize to an appropriate value
            target.Connect(connectionId, target1);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for cmdDown_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("HellfireStudios.StarboundPlanetTracker.exe")]
        public void cmdDown_ClickShould()
        {
            NumberUpDown_Accessor target = new NumberUpDown_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            RoutedEventArgs e = null; // TODO: Initialize to an appropriate value
            target.cmdDown_Click(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for cmdUp_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("HellfireStudios.StarboundPlanetTracker.exe")]
        public void cmdUp_ClickShould()
        {
            NumberUpDown_Accessor target = new NumberUpDown_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            RoutedEventArgs e = null; // TODO: Initialize to an appropriate value
            target.cmdUp_Click(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for txtNum_TextChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("HellfireStudios.StarboundPlanetTracker.exe")]
        public void txtNum_TextChangedShould()
        {
            NumberUpDown_Accessor target = new NumberUpDown_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            TextChangedEventArgs e = null; // TODO: Initialize to an appropriate value
            target.txtNum_TextChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for NumValue
        ///</summary>
        [TestMethod()]
        public void NumValueShould()
        {
            NumberUpDown target = new NumberUpDown(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.NumValue = expected;
            actual = target.NumValue;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
