﻿using HellfireStudios.StarboundPlanetTracker.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Media.Imaging;
using System.Collections.Generic;

namespace HellfireStudios.StarboundPlanetTracker.Tests
{
    
    
    /// <summary>
    ///This is a test class for UIPlanetSkinnerTests and is intended
    ///to contain all UIPlanetSkinnerTests Unit Tests
    ///</summary>
    [TestClass()]
    public class UIPlanetSkinnerTests
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for UIPlanetSkinner Constructor
        ///</summary>
        [TestMethod()]
        public void UIPlanetSkinnerConstructorShouldReturnValidUIPlanerSkinner()
        {
            string ImageRootPath = string.Empty;
            UIPlanetSkinner_Accessor target = new UIPlanetSkinner_Accessor(ImageRootPath);
            Assert.AreEqual(ImageRootPath, target._ImageRootPath);
            Assert.IsTrue(target.Biomes.Count == 0);
            Assert.IsTrue(target.Overlays.Count == 0);
            Assert.IsTrue(target.Patterns.Count == 0);
            Assert.IsNotNull(target.Design);
        }

        /// <summary>
        ///A test for GetBiomeBitmap
        ///</summary>
        [TestMethod()]
        public void GetBiomeBitmapShould()
        {
            string ImageRootPath = string.Empty; // TODO: Initialize to an appropriate value
            UIPlanetSkinner target = new UIPlanetSkinner(ImageRootPath); // TODO: Initialize to an appropriate value
            BitmapImage expected = null; // TODO: Initialize to an appropriate value
            BitmapImage actual;
            actual = target.GetBiomeBitmap();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetBitmapImage
        ///</summary>
        [TestMethod()]
        [DeploymentItem("HellfireStudios.StarboundPlanetTracker.exe")]
        public void GetBitmapImageShould()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            UIPlanetSkinner_Accessor target = new UIPlanetSkinner_Accessor(param0); // TODO: Initialize to an appropriate value
            Uri uri = null; // TODO: Initialize to an appropriate value
            BitmapImage expected = null; // TODO: Initialize to an appropriate value
            BitmapImage actual;
            actual = target.GetBitmapImage(uri);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetOverlayBitmap
        ///</summary>
        [TestMethod()]
        public void GetOverlayBitmapShould()
        {
            string ImageRootPath = string.Empty; // TODO: Initialize to an appropriate value
            UIPlanetSkinner target = new UIPlanetSkinner(ImageRootPath); // TODO: Initialize to an appropriate value
            BitmapImage expected = null; // TODO: Initialize to an appropriate value
            BitmapImage actual;
            actual = target.GetOverlayBitmap();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetPatternBitmap
        ///</summary>
        [TestMethod()]
        public void GetPatternBitmapShould()
        {
            string ImageRootPath = string.Empty; // TODO: Initialize to an appropriate value
            UIPlanetSkinner target = new UIPlanetSkinner(ImageRootPath); // TODO: Initialize to an appropriate value
            BitmapImage expected = null; // TODO: Initialize to an appropriate value
            BitmapImage actual;
            actual = target.GetPatternBitmap();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetUri
        ///</summary>
        [TestMethod()]
        [DeploymentItem("HellfireStudios.StarboundPlanetTracker.exe")]
        public void GetUriShould()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            UIPlanetSkinner_Accessor target = new UIPlanetSkinner_Accessor(param0); // TODO: Initialize to an appropriate value
            string layer = string.Empty; // TODO: Initialize to an appropriate value
            string name = string.Empty; // TODO: Initialize to an appropriate value
            string extension = string.Empty; // TODO: Initialize to an appropriate value
            Uri expected = null; // TODO: Initialize to an appropriate value
            Uri actual;
            actual = target.GetUri(layer, name, extension);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for LoadImagePaths
        ///</summary>
        [TestMethod()]
        [DeploymentItem("HellfireStudios.StarboundPlanetTracker.exe")]
        public void LoadImagePathsShould()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            UIPlanetSkinner_Accessor target = new UIPlanetSkinner_Accessor(param0); // TODO: Initialize to an appropriate value
            target.LoadImagePaths();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for LoadNamesLists
        ///</summary>
        [TestMethod()]
        [DeploymentItem("HellfireStudios.StarboundPlanetTracker.exe")]
        public void LoadNamesListsShould()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            UIPlanetSkinner_Accessor target = new UIPlanetSkinner_Accessor(param0); // TODO: Initialize to an appropriate value
            target.LoadNamesLists();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Biomes
        ///</summary>
        [TestMethod()]
        public void BiomesShould()
        {
            string ImageRootPath = string.Empty; // TODO: Initialize to an appropriate value
            UIPlanetSkinner target = new UIPlanetSkinner(ImageRootPath); // TODO: Initialize to an appropriate value
            List<string> expected = null; // TODO: Initialize to an appropriate value
            List<string> actual;
            target.Biomes = expected;
            actual = target.Biomes;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Design
        ///</summary>
        [TestMethod()]
        public void DesignShould()
        {
            string ImageRootPath = string.Empty; // TODO: Initialize to an appropriate value
            UIPlanetSkinner target = new UIPlanetSkinner(ImageRootPath); // TODO: Initialize to an appropriate value
            PlanetSkin expected = null; // TODO: Initialize to an appropriate value
            PlanetSkin actual;
            target.Design = expected;
            actual = target.Design;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Overlays
        ///</summary>
        [TestMethod()]
        public void OverlaysShould()
        {
            string ImageRootPath = string.Empty; // TODO: Initialize to an appropriate value
            UIPlanetSkinner target = new UIPlanetSkinner(ImageRootPath); // TODO: Initialize to an appropriate value
            List<string> expected = null; // TODO: Initialize to an appropriate value
            List<string> actual;
            target.Overlays = expected;
            actual = target.Overlays;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Patterns
        ///</summary>
        [TestMethod()]
        public void PatternsShould()
        {
            string ImageRootPath = string.Empty; // TODO: Initialize to an appropriate value
            UIPlanetSkinner target = new UIPlanetSkinner(ImageRootPath); // TODO: Initialize to an appropriate value
            List<string> expected = null; // TODO: Initialize to an appropriate value
            List<string> actual;
            target.Patterns = expected;
            actual = target.Patterns;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
