﻿using HellfireStudios.StarboundPlanetTracker.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Win32;
using HellfireADK.Sqlite;
using System.Collections.Generic;

namespace HellfireStudios.StarboundPlanetTracker.Tests
{
    
    
    /// <summary>
    ///This is a test class for UIPlanetTrackerTests and is intended
    ///to contain all UIPlanetTrackerTests Unit Tests
    ///</summary>
    [TestClass()]
    public class UIPlanetTrackerTests
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for UIPlanetTracker Constructor
        ///</summary>
        [TestMethod()]
        public void UIPlanetTrackerConstructorShould()
        {
            UIPlanetTracker target = new UIPlanetTracker();
            Assert.IsNotNull(target);
            Assert.IsNotNull(target.Bookmarks);
            Assert.IsNotNull(target.PlanetsFound);
        }


        [TestMethod()]
        public void GetFilePathFromDialogShould()
        {
            Assert.Inconclusive("No appropriate type parameter is found to satisfies the type constraint(s) of T. " +
                    "Please call GetFilePathFromDialogShouldHelper<T>() with appropriate type paramet" +
                    "ers.");
        }

        /// <summary>
        ///A test for GetFolderPathFromDialog
        ///</summary>
        [TestMethod()]
        public void GetFolderPathFromDialogShould()
        {
            Assert.Inconclusive("This needs to be a coded UI test.");
        }

        /// <summary>
        ///A test for LoadPlanet
        ///</summary>
        [TestMethod()]
        public void LoadPlanetShouldLoadASinglePlanetFromAFilePath()
        {
            Assert.Inconclusive("This needs to be a coded UI test.");
        }

        /// <summary>
        ///A test for LoadPlanets
        ///</summary>
        [TestMethod()]
        public void LoadPlanetsShould()
        {
            SqliteDatabase db = null; // TODO: Initialize to an appropriate value
            List<Planet> expected = null; // TODO: Initialize to an appropriate value
            List<Planet> actual;
            actual = UIPlanetTracker.LoadBookmarkedPlanets(db);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Bookmarks
        ///</summary>
        [TestMethod()]
        public void BookmarksShouldReturnValidList()
        {
            UIPlanetTracker target = new UIPlanetTracker();
            var expected = new List<Planet>()
            {
                new Planet() {Name = "BookMarkPlanet1" },
                new Planet() {Name = "BookMarkPlanet2" },
                new Planet() {Name = "BookMarkPlanet3" },
                new Planet() {Name = "BookMarkPlanet4" },
                new Planet() {Name = "BookMarkPlanet5" }
            };
            target.Bookmarks = expected;
            var actual = target.Bookmarks;
            Assert.ReferenceEquals(expected, actual);
        }

        /// <summary>
        ///A test for PlanetsFound
        ///</summary>
        [TestMethod()]
        public void PlanetsFoundShould()
        {
            UIPlanetTracker target = new UIPlanetTracker();
            var expected = new List<Planet>()
            {
                new Planet() {Name = "FoundPlanet1" },
                new Planet() {Name = "FoundPlanet2" },
                new Planet() {Name = "FoundPlanet3" },
                new Planet() {Name = "FoundPlanet4" },
                new Planet() {Name = "FoundPlanet5" }
            };
            target.PlanetsFound = expected;
            var actual = target.PlanetsFound;
            Assert.ReferenceEquals(expected, actual);
        }
    }
}
