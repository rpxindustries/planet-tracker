﻿using HellfireStudios.StarboundPlanetTracker.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace HellfireStudios.StarboundPlanetTracker.Tests
{
    
    
    /// <summary>
    ///This is a test class for PlanetSkinTests and is intended
    ///to contain all PlanetSkinTests Unit Tests
    ///</summary>
    [TestClass()]
    public class PlanetSkinTests
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for PlanetSkin Constructor
        ///</summary>
        [TestMethod()]
        public void PlanetSkinConstructorShould()
        {
            PlanetSkin target = new PlanetSkin();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for GetPlanetSkinFromString
        ///</summary>
        [TestMethod()]
        public void GetPlanetSkinFromStringShould()
        {
            string design = string.Empty; // TODO: Initialize to an appropriate value
            PlanetSkin expected = null; // TODO: Initialize to an appropriate value
            PlanetSkin actual;
            actual = PlanetSkin.GetPlanetSkinFromString(design);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ToString
        ///</summary>
        [TestMethod()]
        public void ToStringShould()
        {
            PlanetSkin target = new PlanetSkin(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.ToString();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for BiomeName
        ///</summary>
        [TestMethod()]
        public void BiomeNameShould()
        {
            PlanetSkin target = new PlanetSkin(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.BiomeName = expected;
            actual = target.BiomeName;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Height
        ///</summary>
        [TestMethod()]
        public void HeightShould()
        {
            PlanetSkin target = new PlanetSkin(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.Height = expected;
            actual = target.Height;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for OverlayName
        ///</summary>
        [TestMethod()]
        public void OverlayNameShould()
        {
            PlanetSkin target = new PlanetSkin(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.OverlayName = expected;
            actual = target.OverlayName;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for PatternName
        ///</summary>
        [TestMethod()]
        public void PatternNameShould()
        {
            PlanetSkin target = new PlanetSkin(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.PatternName = expected;
            actual = target.PatternName;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Width
        ///</summary>
        [TestMethod()]
        public void WidthShould()
        {
            PlanetSkin target = new PlanetSkin(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.Width = expected;
            actual = target.Width;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
