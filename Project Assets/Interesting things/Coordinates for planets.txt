FROM : ****** http://steamcommunity.com/sharedfiles/filedetails/?id=201938867#-1

A Reminder
This guide is a collection of coordinates I found either by myself or by someone else on the internet, I just putting them here together in one place for easy reading. Think of this as a "Hitch Hiker Guide to Starbound's Galaxy Classic Redux OMG 9001" (Thanks, Ethan XD ) if you will, and please forgive the blandness and low quality-esque of this guide,since this is a "Write as I go" kind of guide.

Also, To save time on my part,I won't include the name of the system and sub-system, just the name of the planet itself since it was pretty obvious which system and sub-system a planet is in.

For Example,If a planet name is...say, "Alpha Grumpy V a" It means that planet is in Alpha Sector,Grumpy System, Grumpy V sub-system.
From me, To you, stuffs I wanna say.
Happy Holidays!

My personal checklist
- Either delete or replace old .world file. 
- Need to find Beta sector gunshop!

Alpha Sector
Let's start off with an obligatory gun shop coordinates

Gun Shop,Wizard Merchant
Notable Find: Gun Shop,Chest Loots: Doctor Spawner
Coord: X -1444486 Y -34877301
Planet: Alpha Kyatz 20 I c (LV1 Forest)
Right from starting point is a Wizard Merchant
Left from starting point pass a cultist Altar is an Airship with the Gun Shop.

In case someone doesn't know how to spot an Air Ship.

Gun Shop
Notable Find: Gun Shop
Coord: X 59877410 Y -70634402
Planet: Alpha Umbraxion 1207 II (LV1 Desert)
Immediately Left from starting point is an Airship.

Random Tech
Notable Find: Random tech blueprint
Coord: X -9338947 Y 32641551
Planet: Alpha Alrescha 4456 I a (LV1 Forest)
Left from starting point, In a Lab at the end of a jumping puzzle, you might find a miniboss along the way.

Random Tech
Notable Find: Random tech blueprint
Coord: X -55578348 Y 31190037
Planet: Alpha Delta Cru 858 I a (LV1 Snow)
Pretty far Left from starting point is a Lab with a random tech blueprint,there are also some bandits along the way,there might be a miniboss too.

Pulse Jump Tech,Hostile Underground USMC Base
Notable Find: Energy Dash Blueprint,An undergound USMC Bunker
Coord: X -60624017 Y -7383438
Planet: Alpha Sigma Ori 910 III b (LV1 Arid)
Left from starting point is an undergound USMC Base,The Blueprint will be in a high-tech chest further left(I found it underwater) 


Friendly Avian Village (Medieval),Rock Organ
Notable Find: Avian Village,several merchants and chests inside the village.
Coord: X 34249815 Y 97767254
Planet: Alpha Omicron Majoris IV b (LV1 Forest)
Right from starting point at the bottom of a valley is a chest with a Rock Organ and some other items inside,further right is the village.

Click here to download .World file

Friendly Floran Village (Primitive)
Notable Find: Floran village and some loot container inside
Coord: X 18081208 Y -17694898
Planet: Alpha Syzgy 128 VI b (LV1 Forest)
Right from starting point pass an Avian house is the Floran village.

Friendly Mushroom Village ( Thanks, Wander )
Notable Find: SHROOOMS! 8D
Coord: X 5822477 Y 79831190
Planet: Alpha Mothallah 006 I b (LV1 Forest)
The Mushroom village is around the starting point,there are also several Mushroom Chests in the houses.

Hard Hat,Hostile Apex Labs (Thanks MRLEMONADEZ123)
Notable Find:A Couple Apex Labs with a lot of loot containers.
Coord: X 38787800 Y -76425534
Planet: Alpha Astrax 5129 VII a (LV1 Forest)
Left from starting point pass a couple of chests is an Apex Lab with a Hard Hat in a chest infront of the lab, and another lab further left.

Click here to download .World file.

Hostile Apex Labs (Thanks lava.drop)
Notable Find: Several hostile Apex labs, several loot containers.
Coord: X 73060118 Y -78030900
Planet: Alpha Carinae 64 IV (LV1 Forest)
Left from starting point are a couple of Hostile Apex Labs, pretty far.

Hostile Glitch Towers,Sewer Dungeon ( Thanks, DerpBulbasaur )
Notable Find: A few Glitch Towers with an npc in each of them,has a chance to drop Crown,Poop Dungeon
Coord: X -13376555 Y -5834797
Planet: Alpha Duania 01 II
Right from starting point there will be a few Glitch Towers along the way,each one of them will have an npc guading secret door inside.
Left from starting point quite far away there will be a Poop dungeon, If this is the first time you visit such place,watch out for small poop monsters.

Hostile USMC Bunker,Random Tech,Doctor Spawner ( Thanks, Sniper Wolf )
Notable Find: A random tech blueprints,a USCM Bunker,Doctor Spawner
Coord: X -7387983 Y -81344325
Planet: Alpha Delta-1 Gru 419 II a (LV1 Forest)
*Left from starting point not too far is a Hight-Tech chest with a random tech blueprint inside (I found Pulse Jump) further left will be a Hostile USMC Bunker,keep going left after the bunker there will be chest with a Doctor-Spawner inside.

Rainbow Cloak Backpack
Notable Find: Rainbow Cloak
Coord: X 68886826 Y 95542822
Planet: Alpha Tania Borealis 618 I (LV1 Desert)
Normally this planet should have a mini rainbow biome with a rainbow chest and the Rainbow Cloak inside,but I'll just place mine on the surface instead, So if you're still looking for a Rainbow Cloak, you can get one here. (Look for a chest in a well lit area)

Click here to download .World file.

Terrifying Wings Backpack
Notable Find: Terrifying Wings,Codexes,Food Recipes
Coord: X -42828669 Y -98825210
Planet: Alpha Vitus Gigantus 46 IV b (LV1 Forst)
On the planet there will be outcast scatter about, these guys drop Terrifying Wings,there are also a few High-Tech chest on the surface,one of them has a Tech Blueprint.

Alpha Sector Cont.
Random Tech,Several Loot Chests,( Thanks, Doge Batman )
Notable Find: Random tech blueprint,
Coord: X 68886826 Y 95542822
Planet: Alpha Tania Borealis 618 I a
Left from starting point there will be a couple of bunkers with robot guards, each one has a chest inside,further left is a Lab with a Jumping puzzle and a High-Tech chest at the end.

Cardboard Hobo Hat,T.V. Helmet,Gun Shops,Superhero Pants ( Thanks, Kaz Miller )
Notable Find: Cardboard Hobo Hat,T.V. Helmet,Gun Shops,and several loot chests
Coord: X 12572825 Y -2896034
Planet Alpha Alrescha 2750 I
Left from starting point you'll find several loot chests one of them has the Hobo Hat.

Keep going left you come across a couple of Airship,the one on the left will have a T.V. Helmet in one chest the other chest will have a Super Hero Pants

Click here to download .world file
Beta Sector
2 Random Tech
Notable Find: 2 Random Tech Blueprints
Coord: X 88180203 Y -91122708
Planet: Beta Coronae Majoris II d (LV2 Jungle Behind Beta Coronae Majoris II f )
Left from starting point,in a chest before you reach a Lab and another one in a chest at the end of a Jumping Puzzle at the Lab.

Wizard Merchant
Notable Find: Wizard Merchant, sells Wizard hat and Stimpacks.
Coord: X 5955002 Y -95997501
Planet: Beta Baastolaan 32 V a (LV2 Forest)
Right from starting point,not too far.

Hostile Underground Base,Good Silver,Gold,Platinum mining
Notable Find: 2 High Tech Chests with musical instruments inside,some Loot Containers with Scientist Clothings 
Coord: X -16014666 Y -12671478
Planet: Beta Gamm Hyi Majoris III a (LV2 Desert)
Left from starting point is the base,very close by,and If you dig down just before you reach the base you should find lots of gold and silver underneath and some platinum.

Friendly Glitch village ( Thanks, BreadRoller?)
Notable Find: Several Loot Chests,Fruit/Veggies Merchants. Hay Pile you can sleep in =D
Coord: X 73118811 Y -82755630
Planet: Beta HD 210277 Aqr 234 II a (LV 2 Jungle)
Left from starting point is a Medieval Glitch village.

Plain Hood, Hostile Floran Prison
Notable Find: A Plain Hood,Floran Prison,some bandit camps,and cultist altars
Coord: X -60442993 Y -11309755
Planet: Beta 10 Uma 161 III
Left from starting point there will be a few chest,The Plain Hood is in the secound chest,and if you keep going you'll find a Floran Prison.

Click here to download .world file
Gamma Sector
Gun Shop,Good Titanium mining spot
Notable Find: Gun Shop Avian Armor blueprint also available.
Coord: X 2 Y -2
Planet: Gamma Shedar Majoris I (LV3 Desert)
Left from starting point on an airship,and if you dig down in an area around starting point deep enough you'll find a lot of Titaniums, enough for a full set and the Decoy princess. Seriously, This planet alone can set you up for Delta no problem.

Friendly Avian Village,Good Amount of Ore on Surface
Notable Find:Avian Village,Chest Items: Codexes,Food Recipes
Coord: X -60 Y -21
Planet Gamma HR 8734 Psc Minoris I (LV3 Jungle)
Left from starting point pass several lone houses are the village,there are some Cultists along the way.
Delta Sector
Gun Shop
Notable Find: A gun shop,a few flesh chests
Coord: X 454545444 Y 454545449
Planet: Delta Tarazed 7346 I (LV4 Jungle)
Left from starting point quite a way away is a few flesh chests,one of them has Flesh item blueprint,further left is an Airship with a Gun Shop

Gun Shop,Mini Bosses,Random Tech ( Thanks, HibernateZ )
Notable Find: A gun shop,a couple of mini bosses,several loot chests,and a random tech blueprint
Coord: X 101010094 Y 101010099
Planet: Delta Matar Majoris III b (LV4 Tundra)
Left from starting point are a couple of chests and mini bosses,further left very far away is an an Air ship with the gunshop in side further left on a mountain under a tiny pool of water will be a High-Tech Chest with a random tech blueprint inside

Wizard Merchant
Notable Find: Wizard Merchant,some diamond ores on the surface.
Coord: X -28 Y -21
Planet: Delta Oswin 56 I b (LV4 Magma)
Left from starting point is a Wizard Merchant

Hostile Dungeon,Several Loot Chests,Mini Bosses
Notable Find: A hostile dungeon, several loot chests,and a few mini bosses
Coord: X 999999999 Y 999999999
Planet: Delta Alkes 596 III (LV4 Grasslands)
Imediatly Right from starting point are 2 Loot Chests one of them has a chance of containing a Legendary Item,Left from starting point is another Loot Chest,further left are a few mini bosses (Possibly Random) and an Underground Base,inside it are a few High Tech chests with random loots and a Matter Block Maker.
If you don't know what a Matter Block Maker is,It's a machine...thingie inside the dungeon that when you activate it it blows up and spew out several purple blocks, those are Matter Blocks, a kind of materials.

Excavation Site,Good Surface Ores
Notable Find: An excavation site with a lot of loot containers,Good surface area ores: Silver,Gold and Titanium are common,sometime Diamond,and Platinum
Coord: X -4 Y 999999999
Planet: Delta Atik 2433 I (LV4 Tentacle)
Left from starting point is the excavation site, Ores are scattered all throughout the surface of the planet.and since the planet are mostly snow and corrupted dirt,you can mine through them quick.

Crown,Bubble Boost and Human Mech Tech ( Thanks, Inanimate Carbon Rod )
Notable Find:A Crown Bubble Boost and Human Mech Tech blueprint,Several Loot Chests
Coord: X 8943261 Y 80666196
Planet: Delta Kappa CrB 4167 I (LV4 Tentacle)
Right from starting point you'll come across several chests along the way,in the first abandoned Apex Structyre you'll find a crown,further right you file a High-Tech chest with the Bubble Boost Tech, keep going until you find another Abandoned Apex Structure there will be another High-Tech chest inside, in which you'll find the Human Mech Tech
Click here to download .World file
X Sector
LV5 Gun Shop ( Thanks, CreeperMaster2 )
Notable Find: LV5 Gun Shop (Also sells Avian armor blueprints),and some loot containers
Coordinates: X -108 Y -66
Planet X Glados 9991 II b (LV5 Jungle)
Left from starting point is an Air Ship with the gun shop inside,very close by.

Targeted Blink Tech,Good X Sector Starting Planet
Notable Find:Targeted Blink tech blueprint,excavation site,good amount of ores.
Coord: X 34249668 Y 97767238
Planet: X HD 195019 Del 128 II a (LV5 Arid)
Right from starting point at the bottom of a dark valley is a High-Tech chest with the Blink tech blueprint in it.

Left from starting point is an Excavation site and if you go in there until you found a pool,dig under that pool, there are good amount Aegisalt and Uranium underneath it.

Bubble Tech,A ROCKET LAUNCHER! Credits: Youtuber Starbrethren
Notable Find: A Bubble Tech blueprint,and a rocket launcher
Coord: X -13 Y -1
Planet: X Edasich I c (LV10 Tundra)
Right from starting point is a High-Tech chest with the Bubble Tech in it,and Left from starting point there will be a chest with the Rocket Launcher in it, Both are really close to the starting point,so you can rush those 2 chest without getting horribly murdered.

Gravity Neutraliser Tech, Wizard Merchant ( Thanks, Kaiachi )
Notable Find: A Gravity Neutraliser Tech blueprint,a Wizard Merchant and several loot chests
Coord: X 88593978 Y -6723008
Planet X Mebsuta Minoris I (LV5 Tentacle)
Right from starting point quite far away there will be a High-Tech chest with the Gravity Neutraliser tech Blueprint inside.

Further right you'll come across a Wizard Tower
And if you keep going you'll find some more chests one of them has a shield looks just like the one I'm using.

Human Mech Tech ( Thanks, Kaiachi )
Notable Find: Human Mech Tech blueprint
Coord: X 88593978 Y -6723008
Planet: X Mebsuta Minoris III (LV9 Snow)
Go right from starting point,the blueprint is in a High-Tech chest,pretty close by

Butterfly Boost Tech ( Thanks, Mr.guy )
Notable Find Butterfly Boost Tech blueprint, a couple of wizard tower,a few Glitch towers,and houses
Coord: X -31 Y 3
Planet: X AI Athfar 0768 III (LV6 Forest)
Left from starting point you'll find a Glitch tower, further left is a Jumping puzzle with the Butterfly Boost Tech blueprint at the end of it,there are also other random goodies on the surface
UPDATES / CREDITS
In case someone don't want to scan the wall of texts for something new in the guide. (Newer stuffs are on top)

- Thanks .ratavA nA, You know what I'm talking about x3
- Finally! a Butterfly Boost Tech!! ( Thanks, Mr.guy )
- Removed .world file for Gravity Neutraliser tech since it's not random
- Shark Hat and Cardboard Hat are gone,but found something else instead,.world file uploaded
- Horse Mask hat is gone,but found 2 different hat instead,no pic for now image upload is broken.
- Revised some obsolete coords in Beta Sector ( Thanks, Toasty Toad, I'll make sure I eat you last!)
- Re-upload .world file for the Rainbow Cloak.
- Crown,Bubble Boost and Human Mech Tech ( Thanks, Inanimate Carbon Rod )
- Delete some obsolete entries thanks to the new patch.
- Human Mech Tech ( Thanks, Kaiachi )
- Bubble Boost Tech ( Thanks, Ethical Lune )
- Gravity Neutraliser Tech ( Thanks, Kaiachi )
- Cardboard Hat ( Thanks, weaselmeasel )
- Added .world file for the Shark Hat.
- A planet with some Glitch Towers and a poop dungeon ( Thanks, DerpBulbasaur )
- A Horse Mask, and a Harp ( Thanks, Kaz Miller )
- A Friendly Mushroom Village ( Thanks, Wander )